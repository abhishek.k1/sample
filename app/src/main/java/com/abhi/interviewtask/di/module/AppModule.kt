package com.indg.almandoos.di.module

import android.app.Application
import android.content.Context
import com.abhi.interviewtask.data.*
import com.abhi.interviewtask.util.AppConstants
import com.abhi.interviewtask.util.AppSchedulerProvider
import com.abhi.interviewtask.util.SchedulerProvider
import com.indg.almandoos.data.remote.ApiHelper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.indg.almandoos.data.remote.AppApiHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {


    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper {
        return appApiHelper
    }


    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }


    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }

    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String {
        return AppConstants.PREF_NAME
    }

    @Provides
    @Singleton
    internal fun providePreferencesHelper(appPreferencesHelper: AppPreferencesHelper): PreferencesHelper {
        return appPreferencesHelper
    }


    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }

}
