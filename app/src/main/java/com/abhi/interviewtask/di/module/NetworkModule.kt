package com.indg.almandoos.di.module

import com.abhi.interviewtask.util.AppConstants.Companion.BASE_URL
import com.indg.almandoos.data.remote.ApiEndPoint
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.xml.datatype.DatatypeConstants.SECONDS
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


@Module
object NetworkModule {

    @Reusable
    @Provides
    @JvmStatic
    internal fun providePostApi(retrofit: Retrofit): ApiEndPoint
    {
        return retrofit.create(ApiEndPoint::class.java)
    }


    var okHttpClient = OkHttpClient.Builder()
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .build()

    @Provides
    @JvmStatic
    @Reusable
    internal fun provideRetrofitInterface(): Retrofit
    {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

    }


}