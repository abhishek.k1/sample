package com.indg.almandoos.di.builder



import com.abhi.interviewtask.ui.activity.heartrate.HeartRateActivity
import com.abhi.interviewtask.viewmodel.calories.CaloriesModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {


    @ContributesAndroidInjector(modules = ([CaloriesModule::class]))
    abstract fun bindviewModel() : HeartRateActivity

}
