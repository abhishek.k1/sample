package com.indg.almandoos.di.component

import android.app.Application
import com.abhi.interviewtask.MainApplication
import com.indg.almandoos.di.builder.ActivityBuilder
import com.indg.almandoos.di.module.AppModule
import com.indg.almandoos.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class,NetworkModule::class, AppModule::class, ActivityBuilder::class])
interface AppComponent {

    fun inject(app: MainApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}