package com.abhi.interviewtask.data

interface PreferencesHelper {

    var accessToken: String

    var currentUserEmail: String


    var currentUserName: String

    var currentUserProfilePicUrl: String

}
