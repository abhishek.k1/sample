package com.indg.almandoos.data.remote

import com.abhi.interviewtask.data.model.ExcerciseModelResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.HeaderMap
import retrofit2.http.POST


interface ApiEndPoint {

    @FormUrlEncoded
    @POST("/v2/natural/exercise")
    fun GetNutrionExcerciseResponse(@HeaderMap headerData: HashMap<String, String>, @Field("query") query:String): Call<ExcerciseModelResponse>


}