package com.abhi.interviewtask.data

import android.content.Context
import com.google.gson.Gson
import com.indg.almandoos.data.remote.ApiHelper
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class AppDataManager @Inject
constructor(
    private val mContext: Context,
    private val mApiHelper: ApiHelper,
    private val mPreferencesHelper: PreferencesHelper,
    private val mGson: Gson
) : DataManager {


    override var accessToken: String
        get() = mPreferencesHelper.accessToken
        set(accessToken) {
            mPreferencesHelper.accessToken = accessToken!!
        }



    override var currentUserEmail: String
        get() = mPreferencesHelper.currentUserEmail
        set(email) {
            mPreferencesHelper.currentUserEmail = email!!
        }




    override var currentUserName: String
        get() = mPreferencesHelper.currentUserName
        set(userName) {
            mPreferencesHelper.currentUserEmail = userName!!
        }

    override var currentUserProfilePicUrl: String
        get() = mPreferencesHelper.currentUserProfilePicUrl
        set(profilePicUrl) {
            mPreferencesHelper.currentUserProfilePicUrl = profilePicUrl!!
        }






    override fun updateUserInfo(
        accessToken: String,
        userName: String,
        email: String,
        profilePicPath: String
    ) {

        this.accessToken = accessToken
        currentUserName = userName
        currentUserEmail = email
        currentUserProfilePicUrl = profilePicPath

    }
}