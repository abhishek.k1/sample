package com.abhi.interviewtask.data


import com.indg.almandoos.data.remote.ApiHelper

/**
 * Created by Abhishek on 13/02/20.
 */
interface DataManager :  PreferencesHelper , ApiHelper {

    fun updateUserInfo(
        accessToken: String,
        userName: String,
        email: String,
        profilePicPath: String
    )


}