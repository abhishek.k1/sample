package com.abhi.interviewtask.data.remote

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier


/**
 * Created by Abhishek on 13/02/20.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class ApiInfo