package com.abhi.interviewtask.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(
    entities = [LoginTable::class],
    version = 1,
    exportSchema = false
)
abstract class LoginDatabase : RoomDatabase() {
    abstract fun loginDoa(): LoginDao?

    companion object {
        private var INSTANCE: LoginDatabase? = null
        fun getDatabase(context: Context?): LoginDatabase? {
            if (INSTANCE == null) {
                synchronized(LoginDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context!!, LoginDatabase::class.java, "LOGIN_DATABASE"
                        )
                            .fallbackToDestructiveMigration()
                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}