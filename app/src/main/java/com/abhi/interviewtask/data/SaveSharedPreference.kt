package com.abhi.interviewtask.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.abhi.interviewtask.util.AppConstants.Companion.kInitialStepCount
import com.abhi.interviewtask.util.AppConstants.Companion.kStartDate
import java.util.*

object SaveSharedPreference {


    private fun getPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }


    fun setStartDate(context: Context, date: Date) {
        val editor = getPreferences(context).edit()
        editor.putLong(kStartDate, date.time)
        editor.apply()
    }

    fun setInitialStepCount(context: Context, step: Int) {
        val editor = getPreferences(context).edit()
        editor.putInt(kInitialStepCount, step)
        editor.apply()
    }


    fun getStartDate(context: Context): Date? {
        val time = getPreferences(context).getLong(kStartDate, 0L)
        if (time != 0L) {
            return Date(time)
        }
        return null
    }

    fun clear(context: Context) {
        getPreferences(context).edit().clear()
    }


    fun getInitialStepcount(context: Context): Int {
        return getPreferences(context).getInt(kInitialStepCount, 0)
    }


}