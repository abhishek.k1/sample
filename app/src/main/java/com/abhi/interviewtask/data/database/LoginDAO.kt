package com.abhi.interviewtask.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface LoginDao {
    @Insert
    fun insertDetails(data: LoginTable?)

    @get:Query(
        "select * from LoginDetails"
    )
    val details: LiveData<List<LoginTable?>?>?

    @Query("delete from LoginDetails")
    fun deleteAllData()
}

