package com.abhi.interviewtask.data.model

data class ExcerciseModelResponse(
        val exercises: List<Exercise>
)