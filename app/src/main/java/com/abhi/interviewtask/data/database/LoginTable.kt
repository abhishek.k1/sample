package com.abhi.interviewtask.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity

import androidx.room.PrimaryKey


@Entity(tableName = "LoginDetails")
class LoginTable {
    // Creating Setter and Getter is important
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    var id = 0
    @ColumnInfo(name = "Email")
    var email: String? = null
    @ColumnInfo(name = "Password")
    var password: String? = null

}

