package com.abhi.interviewtask.data.model

data class Photo(
        val highres: String,
        val is_user_uploaded: Boolean,
        val thumb: String
)