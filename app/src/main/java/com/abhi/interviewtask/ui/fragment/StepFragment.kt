package com.abhi.interviewtask.ui.fragment


import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders

import com.abhi.interviewtask.R
import com.abhi.interviewtask.data.SaveSharedPreference
import com.abhi.interviewtask.util.AppUtil
import com.app.progresviews.ProgressWheel
import kotlinx.android.synthetic.main.fragment_step.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class StepFragment : Fragment(), SensorEventListener {

    private var isStarted = false
    private var sensorManager: SensorManager? = null
    private var stepCounter = 0
    private var counterSteps = 0
    private var stepDetector = 0
    private var startDate: Date? = null
    private var wheelprogressid:ProgressWheel?=null
    private var endDate: Date? = null
    companion object {
        fun newInstance() = StepFragment()
        const val TAG = "MainFragment"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = inflater.inflate(R.layout.fragment_step, container, false)
        wheelprogressid = view.findViewById(R.id.wheelprogress)
        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sensorManager = context?.getSystemService(Context.SENSOR_SERVICE) as SensorManager

        // load previous status if any
        startDate = SaveSharedPreference.getStartDate(activity!!)
        counterSteps = SaveSharedPreference.getInitialStepcount(activity!!)

        Log.d(TAG, "startDate = $startDate")
        Log.d(TAG, "counterSteps = $counterSteps")
        if (startDate != null) {
            isStarted = true
        }

        initUI()
        bindUI()
    }

    override fun onResume() {
        super.onResume()
        if (this.isStarted) {
            startService()
        }
    }

    override fun onPause() {
        super.onPause()
        stopService()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        Log.d(TAG, "accuracy = $accuracy")
    }

    override fun onSensorChanged(event: SensorEvent) {
        when (event.sensor.type) {
            Sensor.TYPE_STEP_DETECTOR -> {
                stepDetector++
            }
            Sensor.TYPE_STEP_COUNTER -> {
                if (counterSteps < 1) {
                    counterSteps = event.values[0].toInt()
                    SaveSharedPreference.setInitialStepCount(activity!!,counterSteps)
                }
                stepCounter = event.values[0].toInt() - counterSteps
            }
        }
        wheelprogressid!!.setStepCountText(stepCounter.toString())
    }

    private fun initUI() {
        startDate?.let {
            tv_start_time.text = getString(R.string.start_time, AppUtil.getFormattedDate(it))
        } ?: run {
            tv_start_time.text = getString(R.string.start_time, "-")
        }

        tv_end_time.text = getString(R.string.end_time, "-")
        wheelprogressid!!.setStepCountText(stepCounter.toString())

        if (this.isStarted) {
            btn_start.text = getString(R.string.stop)
        } else {
            btn_start.text = getString(R.string.start)
        }
    }

    private fun bindUI() {
        btn_start.setOnClickListener {
            if (this.isStarted) {
                stopCounter()
                btn_start.text = getString(R.string.start)
            } else {
                startCounter()
                btn_start.text = getString(R.string.stop)
            }
        }
    }

    private fun startService() {
        val stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)

        if (stepsSensor == null) {
            Toast.makeText(context, "No Step Counter Sensor !", Toast.LENGTH_SHORT).show()
        } else {
            sensorManager?.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_UI)
        }
    }

    private fun stopService() {
        sensorManager?.unregisterListener(this)
    }

    private fun startCounter() {
        isStarted = true
        this.stepCounter = 0
        this.stepDetector = 0
        this.counterSteps = 0

        // reset UI
        initUI()

        startDate = Date()
        startDate?.let {
            tv_start_time.text =
                getString(R.string.start_time, AppUtil.getFormattedDate(it))
            SaveSharedPreference.setStartDate(activity!!,it)
        }

        startService()
    }

    private fun stopCounter() {
        Log.d(TAG, "stopCounter")
        isStarted = false
        endDate = Date()
        endDate?.let {
            tv_end_time.text =
                getString(R.string.end_time, AppUtil.getFormattedDate(it))
        }

        stopService()
        SaveSharedPreference.clear(activity!!)
    }
}

