package com.abhi.interviewtask.ui.activity.footstep

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.abhi.interviewtask.R
import com.abhi.interviewtask.data.SaveSharedPreference
import com.abhi.interviewtask.ui.fragment.StepFragment
import kotlinx.android.synthetic.main.activity_main2.*
import java.util.*


class MainActivity : AppCompatActivity(),
    SensorEventListener {
    var tv_steps: TextView? = null
    var sensorManager: SensorManager? = null
    var sensor: Sensor? = null
    private var stepCounter = 0
    private var startDate: Date? = null
    private var counterSteps = 0
    private var isStarted = false
    var running = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp)
        toolbar.setNavigationOnClickListener { arrow -> onBackPressed() }
        title = "Pedometer"
        tv_steps = findViewById(R.id.tv_steps) as TextView
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        // load previous status if any
        startDate = SaveSharedPreference.getStartDate(this)
        counterSteps = SaveSharedPreference.getInitialStepcount(this)

        Log.d(StepFragment.TAG, "startDate = $startDate")
        Log.d(StepFragment.TAG, "counterSteps = $counterSteps")
        if (startDate != null) {
            isStarted = true
        }
        initUI()
    }

    private fun initUI() {
        tv_steps!!.text = stepCounter.toString()

    }


    override fun onResume() {
        super.onResume()
        running = true
        val countSensor =
            sensorManager!!.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        if (countSensor != null) {
            sensorManager!!.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI)
        } else {
            Toast.makeText(this, "SENSOR NOT FOUND", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        running = false
        //if you unregister the hardware will stop detecting steps
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (running) {
            if (counterSteps < 1) {
                counterSteps = event.values[0].toInt()
                SaveSharedPreference.setInitialStepCount(this, counterSteps)
            }
            stepCounter = event.values[0].toInt() - counterSteps
            tv_steps!!.text = stepCounter.toString()
        }
    }

    override fun onAccuracyChanged(
        sensor: Sensor,
        accuracy: Int
    ) {
    }
}

