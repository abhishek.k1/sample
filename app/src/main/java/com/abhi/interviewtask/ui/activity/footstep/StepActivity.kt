package com.abhi.interviewtask.ui.activity.footstep

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.abhi.interviewtask.R
import com.abhi.interviewtask.ui.fragment.StepFragment
import kotlinx.android.synthetic.main.activity_step.*

class StepActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp)
        toolbar.setNavigationOnClickListener { arrow -> onBackPressed() }
        title = "Pedometer"
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, StepFragment.newInstance())
                .commitNow()
        }
    }
}
