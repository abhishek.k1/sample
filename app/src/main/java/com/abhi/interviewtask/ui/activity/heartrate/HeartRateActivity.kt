package com.abhi.interviewtask.ui.activity.heartrate

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.abhi.interviewtask.BR
import com.abhi.interviewtask.R
import com.abhi.interviewtask.base.BaseActivity
import com.abhi.interviewtask.data.model.ExcerciseModelResponse
import com.abhi.interviewtask.data.navigator.ResponseNavigator
import com.abhi.interviewtask.databinding.ActivityHeartRateBinding
import com.abhi.interviewtask.di.module.NutritionRetrofit
import com.abhi.interviewtask.viewmodel.calories.Caloriesviewmodel
import com.indg.almandoos.data.remote.ApiEndPoint
import kotlinx.android.synthetic.main.activity_heart_rate.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class HeartRateActivity : BaseActivity<ActivityHeartRateBinding>(),
    View.OnClickListener {


    @Inject
    lateinit var viewModel: Caloriesviewmodel
    private var binding: ActivityHeartRateBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        binding = viewDataBinding
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_black_24dp)
        toolbar.setNavigationOnClickListener { arrow -> onBackPressed() }
        title= "Nutrition Tracker"
        calorie_save_tv.setOnClickListener(this)

    }

    override val bindingVariable: Int
        get() = BR._all
    override val layoutId: Int
        get() = R.layout.activity_heart_rate

    fun OnSuccess(`object`: Any) {
        hideLoading()
        if (`object` is ExcerciseModelResponse) {
            var data = `object` as ExcerciseModelResponse
            if (!data.exercises.isNullOrEmpty()) {
                var name = data.exercises[0].name + " " + data.exercises[0].duration_min + " min"
                var finalcal = data.exercises[0].nf_calories
                wheelprogress.setStepCountText(finalcal)
            }

        }
    }


    fun OnError(msg: String) {
        hideLoading()
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.calorie_save_tv -> {
                getData()
            }
        }
    }


    private fun getData() {
        if (!query.text.isNullOrEmpty()) {
            showLoading()
            Callapi(query.text.toString())
        } else {
            Toast.makeText(this, "Query Required", Toast.LENGTH_SHORT).show()

        }
    }

    private fun Callapi(name: String) {
        val map = HashMap<String, String>()
        map["x-app-id"] = "2131d777"
        map["x-app-key"] = "93e5443127f263c2dac855ede6d47920"
        val service = NutritionRetrofit.getRetrofitInstance()!!.create(ApiEndPoint::class.java)
        val call = service!!.GetNutrionExcerciseResponse(map, name)
        call.enqueue(object : Callback<ExcerciseModelResponse> {
            override fun onResponse(
                call: Call<ExcerciseModelResponse>,
                response: Response<ExcerciseModelResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body()!!.exercises.isNotEmpty()) {
                        OnSuccess(response.body()!!)
                    } else {
                        OnError("No Data Found")
                    }
                }
            }

            override fun onFailure(call: Call<ExcerciseModelResponse>, t: Throwable) {
                OnError("Something Went Wrong")
            }
        })

    }


}