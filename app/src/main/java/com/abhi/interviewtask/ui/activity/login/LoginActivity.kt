package com.abhi.interviewtask.ui.activity.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.abhi.interviewtask.R
import com.abhi.interviewtask.data.database.LoginTable
import com.abhi.interviewtask.databinding.ActivityLoginBinding
import com.abhi.interviewtask.ui.activity.dashboard.DashboardActivity
import com.abhi.interviewtask.ui.activity.signup.SignupActivity
import com.abhi.interviewtask.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private var loginViewModel: LoginViewModel? = null

    private var binding: ActivityLoginBinding? = null
    var loginlist: List<LoginTable>? = null
    private var loginval = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        loginViewModel = ViewModelProviders.of(this).get(
            LoginViewModel::class.java
        )

        loginViewModel!!.getGetAllData()
            .observe(this,
                Observer<List<LoginTable?>?> {
                    loginlist = it as List<LoginTable>?
                })

        login.setOnClickListener(this)
        register.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.login -> {
                if (!username.text.isNullOrEmpty()) {
                    if (!password.text.isNullOrEmpty()) {
                        if (!loginlist.isNullOrEmpty()) {
                            for (i in loginlist!!.indices) {
                                loginval =
                                    loginlist!![i].email.equals(username.text.toString()) && loginlist!![i].password.equals(
                                        password.text.toString()
                                    )
                            }

                            if (loginval) {
                                var intent = Intent(this,DashboardActivity::class.java)
                                startActivity(intent)
                                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)

                            } else {
                                Toast.makeText(this, "Credential is Invalid", Toast.LENGTH_SHORT).show()

                            }
                        } else {
                            Toast.makeText(this, "Please Register", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this, "Password Required", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Email ID Required", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.register ->{
                var intent = Intent(this,SignupActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
