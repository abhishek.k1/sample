package com.abhi.interviewtask.ui.activity.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.abhi.interviewtask.R
import com.abhi.interviewtask.ui.activity.login.LoginActivity

class SplashActivity : AppCompatActivity() {


    private var SPLASH_DISPLAY_LENGTH = 3500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            try{
               var intent = Intent(this,
                   LoginActivity::class.java)
                startActivity(intent)
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
            }catch (ex:Exception){}
        },SPLASH_DISPLAY_LENGTH.toLong())
    }
}
