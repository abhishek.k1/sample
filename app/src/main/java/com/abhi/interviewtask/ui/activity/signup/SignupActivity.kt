package com.abhi.interviewtask.ui.activity.signup

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.abhi.interviewtask.R
import com.abhi.interviewtask.data.database.LoginTable
import com.abhi.interviewtask.databinding.ActivityLoginBinding
import com.abhi.interviewtask.databinding.ActivitySignupBinding
import com.abhi.interviewtask.ui.activity.login.LoginActivity
import com.abhi.interviewtask.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.view.*


class SignupActivity : AppCompatActivity(), View.OnClickListener {


    private var loginViewModel: LoginViewModel? = null

    private var binding: ActivitySignupBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        loginViewModel = ViewModelProviders.of(this).get(
            LoginViewModel::class.java
        )


        login.setOnClickListener(this)
        signin1.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.login -> {
                if (!email.text.isNullOrEmpty()) {
                    if (isValidEmail(email.text)) {
                        if (!password.text.isNullOrEmpty()) {
                            val data = LoginTable()
                            data.email = email.text.toString()
                            data.password = password.text.toString()
                            loginViewModel!!.insert(data)
                            Toast.makeText(this, "Registered Successfully", Toast.LENGTH_SHORT)
                                .show()
                            var intent = Intent(this, LoginActivity::class.java)
                            intent.flags =
                                (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)

                        } else {
                            Toast.makeText(this, "Password is Required", Toast.LENGTH_SHORT).show()

                        }
                    } else {
                        Toast.makeText(this, "Email ID is not valid", Toast.LENGTH_SHORT).show()

                    }
                } else {
                    Toast.makeText(this, "Email ID is Required", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.signin1->{
                var intent = Intent(this, LoginActivity::class.java)
                intent.flags =
                    (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }

        }
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}
