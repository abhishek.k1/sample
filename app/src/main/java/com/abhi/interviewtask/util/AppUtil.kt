package com.abhi.interviewtask.util

import java.text.SimpleDateFormat
import java.util.*

object AppUtil {

    fun getFormattedDate(date: Date): String {
        val sdf = SimpleDateFormat("yyyy-M-dd HH:mm:ss", Locale.TAIWAN)
        return sdf.format(date)
    }
}