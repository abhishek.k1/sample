package com.abhi.interviewtask.util

/**
 * Created by Abhishek on 13/02/20.
 */
class AppConstants private constructor() {

    companion object {
        const val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"
        const val kStartDate = "startdate"
        const val kInitialStepCount = "stepcount"
        var PREF_NAME = "Interview"
        var BASE_URL = "https://trackapi.nutritionix.com"

    }
}