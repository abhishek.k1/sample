package com.abhi.interviewtask.util

import io.reactivex.Scheduler

/**
 * Created by Abhishek on 13/02/20.
 */
interface SchedulerProvider {
    fun computation(): Scheduler?
    fun io(): Scheduler?
    fun ui(): Scheduler?
}