package com.abhi.interviewtask.util

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by Abhishek on 13/02/20.
 */
object NetworkUtils {

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm != null) {
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }
        return false
    }
}// This class is not publicly instantiable
