package com.abhi.interviewtask.viewmodel.calories

import com.abhi.interviewtask.base.BaseViewModel
import com.abhi.interviewtask.data.DataManager
import com.abhi.interviewtask.data.model.ExcerciseModelResponse
import com.abhi.interviewtask.data.navigator.ResponseNavigator
import com.abhi.interviewtask.util.SchedulerProvider
import com.indg.almandoos.data.remote.ApiEndPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Caloriesviewmodel(
    dataManager: DataManager,
    apiService: ApiEndPoint,
    schedulerProvider: SchedulerProvider
) : BaseViewModel<ResponseNavigator>(dataManager, apiService, schedulerProvider) {


    fun GetNutritionExcerciseList(name: String) {
        val map = HashMap<String, String>()
        map["x-app-id"] = "2131d777"
        map["x-app-key"] = "93e5443127f263c2dac855ede6d47920"
        val call = getApiService()!!.GetNutrionExcerciseResponse(map, name)
        call.enqueue(object : Callback<ExcerciseModelResponse> {
            override fun onResponse(call: Call<ExcerciseModelResponse>, response: Response<ExcerciseModelResponse>) {
                if (response.isSuccessful) {
                    if(response.body()!!.exercises.isNotEmpty()){
                        getNavigator()!!.OnSuccess(response.body()!!)
                    }else{
                        getNavigator()!!.OnError("No Data Found")
                    }
                }
            }

            override fun onFailure(call: Call<ExcerciseModelResponse>, t: Throwable) {
                getNavigator()!!.OnError("Something Went Wrong")
            }
        })


    }


}
