package com.abhi.interviewtask.viewmodel

import android.app.Application
import androidx.annotation.NonNull
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.abhi.interviewtask.data.database.LoginTable
import com.abhi.interviewtask.ui.activity.login.LoginRepository


class LoginViewModel(@NonNull application: Application?) :
    AndroidViewModel(application!!) {


    private val repository: LoginRepository = LoginRepository(application)

    private val getAllData: LiveData<List<LoginTable>>

    fun insert(data: LoginTable?) {
        repository.insertData(data)
    }

    fun getGetAllData(): LiveData<List<LoginTable>> {
        return getAllData
    }

    init {
        getAllData = repository.allData
    }
}