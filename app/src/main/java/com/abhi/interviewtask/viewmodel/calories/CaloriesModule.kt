package com.abhi.interviewtask.viewmodel.calories

import com.abhi.interviewtask.data.DataManager
import com.abhi.interviewtask.util.SchedulerProvider
import com.indg.almandoos.data.remote.ApiEndPoint
import dagger.Module
import dagger.Provides




@Module
class CaloriesModule {
    @Provides
    fun ProvideViewModel(
        dataManager: DataManager,
        apiEndPoint: ApiEndPoint,
        schedulerProvider: SchedulerProvider
    ): Caloriesviewmodel {
        return Caloriesviewmodel(dataManager, apiEndPoint, schedulerProvider)
    }

}
